# frtb

The Fundamental Review of the Trading Book (FRTB) is a new Basel committee framework for the next generation market risk regulatory capital rules. It is inspired by the undercapitalisation of trading book exposures witnessed during the financial cris